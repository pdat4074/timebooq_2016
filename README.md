# README #

### What is this repository for? ###

This repository is for the ELEC5619 Object Oriented Application Frameworks project for 2016.

### How do I get set up? ###

This code is built using Spring Framework 4.3.3.RELEASE

This application requires the use of a database. The connection parameters are given in the following files which require to be changed based on your database settings

/src/main/resources/beans.xml

/src/main/resources/database.properties

The MySql database is stored in

/src/main/resources/timebooq.sql

In order to run the code, ensure you have a version of Apache Tomcat (preferably, 8.0) installed and configured for use on your machine.

Inside STS, simply right-click on the project and choose the Tomcat server you are running to run the code.

The site should run on localhost:8080/app/.

### Who do I talk to? ###

Owner: Prathamesh Datar
email: pdat4074@uni.sydney.edu.au