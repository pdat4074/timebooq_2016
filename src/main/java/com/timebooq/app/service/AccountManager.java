package com.timebooq.app.service;

import java.io.Serializable;
import java.util.List;

import com.timebooq.app.domain.Account.Account;

public interface AccountManager extends Serializable{
	
	public void setCredentials(String username, String password);
	
	public List<Account> getAccounts();

}
