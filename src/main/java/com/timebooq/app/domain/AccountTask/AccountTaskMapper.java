package com.timebooq.app.domain.AccountTask;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AccountTaskMapper implements RowMapper<AccountTask> {
   public AccountTask mapRow(ResultSet rs, int rowNum) throws SQLException {
      AccountTask accountTask = new AccountTask();
      accountTask.setId(rs.getInt("id"));
      accountTask.setName(rs.getString("name"));
      accountTask.setDue(rs.getDate("due"));
      accountTask.setDescription(rs.getString("description"));
      accountTask.setStatus(rs.getString("status"));
      accountTask.setActive(rs.getBoolean("active"));
      return accountTask;
   }
}