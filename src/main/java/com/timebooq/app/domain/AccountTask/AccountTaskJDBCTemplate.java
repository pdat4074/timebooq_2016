package com.timebooq.app.domain.AccountTask;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jca.cci.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.timebooq.app.domain.Account.Account;
import com.timebooq.app.domain.Account.AccountMapper;

public class AccountTaskJDBCTemplate implements AccountTaskDao {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
   private DataSource dataSource;
   private JdbcTemplate jdbcTemplateObject;
   
   public void setDataSource(DataSource dataSource) {
      this.dataSource = dataSource;
      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
   }

   public Boolean newAccountTask(final Long account_id, final String name, final String due, final String description){
	   
	   final String SQL = "insert into account_task (account_id, name, due, description) values (?, ?, ?, ?)";
	   try
	      {
		   		jdbcTemplateObject.update( SQL, account_id, name, due, description);
		   		return true;
	      }
	      catch (InvalidResultSetAccessException e) 
	      {
	          return true;
	      } 
	      catch (DataAccessException e)
	      {
	          return true;
	      }
      
   }
   
   public AccountTask getAccountTaskById(Integer id) {
      String SQL = "select * from account_task where id = ?";
      try
      {
    	  AccountTask accountTask = jdbcTemplateObject.queryForObject(SQL, 
                  new Object[]{id}, new AccountTaskMapper());
    	  	return accountTask; 
      }
      catch (InvalidResultSetAccessException e) 
      {
          return null;
      } 
      catch (DataAccessException e)
      {
          return null;
      }
      
   }
   
   public AccountTask setTaskComplete(Integer id) {
      String SQL = "update account_task set status='Complete' where id = ?";
      try
      {
    	  AccountTask accountTask = jdbcTemplateObject.queryForObject(SQL, 
                  new Object[]{id}, new AccountTaskMapper());
    	  	return accountTask; 
      }
      catch (InvalidResultSetAccessException e) 
      {
          return null;
      } 
      catch (DataAccessException e)
      {
          return null;
      }
      
   }
   
   public List<AccountTask> getAccountTasks(Long account_id) {
	   
      String SQL = "select * from account_task where account_id = ? and active=1";
      try
      {
    	  List<AccountTask> accountTasks = new ArrayList<AccountTask>();
    	  List<Map<String, Object>> rows = jdbcTemplateObject.queryForList(SQL, new Object[]{account_id});
    	  for(Map row: rows){
    		  
    		  AccountTask accountTask = new AccountTask();
    		  accountTask.setId((Long)row.get("id"));
    		  accountTask.setName((String)row.get("name"));
    		  accountTask.setDue((Date)row.get("due"));
    		  accountTask.setDescription((String)row.get("description"));
    		  accountTask.setStatus((String)row.get("status"));
    		  accountTasks.add(accountTask);
    		  
    	  }
    	  
    	  return accountTasks;
      }
      catch (InvalidResultSetAccessException e) 
      {
          return null;
      } 
      catch (DataAccessException e)
      {
          return null;
      }
      
   }

}