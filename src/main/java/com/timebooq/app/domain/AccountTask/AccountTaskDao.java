package com.timebooq.app.domain.AccountTask;

import java.util.Date;
import java.util.List;
import javax.sql.DataSource;

import com.timebooq.app.domain.Account.Account;

public interface AccountTaskDao {

    public Boolean newAccountTask(Long account_id, String name, String due, String description);
    
    public AccountTask getAccountTaskById(Integer id);
    
    public AccountTask setTaskComplete(Integer id);
    
    public List<AccountTask> getAccountTasks(Long id);

}