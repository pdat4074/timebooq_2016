package com.timebooq.app.domain.Account;

import java.util.List;
import javax.sql.DataSource;

import com.timebooq.app.domain.Account.Account;

public interface AccountDao {

    public Account newAccount(String name, String username, String password);
    
    public Account getAccountByUsername(String username);
    
    public Account testCredentials(String username, String password);

}