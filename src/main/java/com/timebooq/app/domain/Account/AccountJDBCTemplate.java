package com.timebooq.app.domain.Account;

import java.util.List;
import javax.sql.DataSource;

import org.springframework.dao.DataAccessException;
import org.springframework.jca.cci.InvalidResultSetAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

public class AccountJDBCTemplate implements AccountDao {
   private DataSource dataSource;
   private JdbcTemplate jdbcTemplateObject;
   
   public void setDataSource(DataSource dataSource) {
      this.dataSource = dataSource;
      this.jdbcTemplateObject = new JdbcTemplate(dataSource);
   }

   public Account newAccount(String name, String username, String password) {
      String SQL = "insert into account (name, username, password) values (?, ?, ?)";
      
      jdbcTemplateObject.update( SQL, name, username, password);
      System.out.println("Created Record Name = " + name + " Username = " + username + " Password: "+ password);
      
      Account account = this.getAccountByUsername(username);
      
      return account;
      
   }

   public Account getAccountByUsername(String username) {
      String SQL = "select * from account where username = ?";
      try
      {
    	  Account account = jdbcTemplateObject.queryForObject(SQL, 
                  new Object[]{username}, new AccountMapper());
    	  	return account; 
      }
      catch (InvalidResultSetAccessException e) 
      {
          return null;
      } 
      catch (DataAccessException e)
      {
          return null;
      }
      
   }
   
   public Account testCredentials(String username, String password) {
	   
	   String SQL = "select * from account where username = ? and password = ?";
	      try
	      {
	    	  Account account = jdbcTemplateObject.queryForObject(SQL, 
	                  new Object[]{username, password}, new AccountMapper());
	    	  	return account; 
	      }
	      catch (InvalidResultSetAccessException e) 
	      {
	          return null;
	      } 
	      catch (DataAccessException e)
	      {
	          return null;
	      }
	      
   }

}