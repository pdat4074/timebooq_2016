package com.timebooq.app.domain.Account;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AccountMapper implements RowMapper<Account> {
   public Account mapRow(ResultSet rs, int rowNum) throws SQLException {
      Account account = new Account();
      account.setId(rs.getInt("id"));
      account.setName(rs.getString("name"));
      account.setUsername(rs.getString("username"));
      account.setPassword(rs.getString("password"));
      return account;
   }
}