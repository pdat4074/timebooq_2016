package com.timebooq.app.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.ModelMap;

import com.timebooq.app.domain.Account.Account;
import com.timebooq.app.domain.Account.AccountJDBCTemplate;
import com.timebooq.app.service.AccountManager;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping("/")
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	private AccountManager accountManager;
	
	private Facebook facebook;
    private ConnectionRepository connectionRepository;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(HttpSession session, Locale locale, Model model) {
		
		Long account_id = (Long)session.getAttribute("account_id");
		if( account_id != null){
			String username = (String)session.getAttribute("account_username");
			model.addAttribute("redirect_url","/account/");
			return "redirect";
		}else{			
			return "login";
		}
		
		
	}
	
	public HomeController(){}

    public HomeController(Facebook facebook, ConnectionRepository connectionRepository) {
        this.facebook = facebook;
        this.connectionRepository = connectionRepository;
    }

    @RequestMapping(value = "/facebook", method = RequestMethod.GET)
    public String helloFacebook(Model model) {
    	
    	try{
    		if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
                return "redirect:/connect/facebook";
            }
    	}catch(NullPointerException e){
    		return "redirect:/connect/facebook";
    	}

        model.addAttribute("facebookProfile", facebook.userOperations().getUserProfile());
        PagedList<Post> feed = facebook.feedOperations().getFeed();
        model.addAttribute("feed", feed);
        return "hello";
    }
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpSession session, Model model){
		
		String error_message = (String)session.getAttribute("error_message");
		if( error_message != ""){
			model.addAttribute("error_message",error_message);
			session.removeAttribute("error_message");
		}
		
		Long account_id = (Long)session.getAttribute("account_id");
		if( account_id != null){
			String username = (String)session.getAttribute("account_username");
			model.addAttribute("redirect_url","/account/");
			return "redirect";
		}else{			
			return "login";
		}
		
	}
	
	@RequestMapping(value = "/account", method = RequestMethod.GET)
	public String get_account(HttpSession session, Model model){
		
		//Set the default status
		Boolean success = true;
		String message = "User account was successfully displayed";
		Account account = null;
		
		if(success == true){
			
			//We can only access this function if a user is logged in
			Long account_id = (Long)session.getAttribute("account_id");
			if( account_id == null){
				success = false;
				message = "User authentication required.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
			}
			
		}
		
		if(success == true){
			
			//This means that a user account is currently authenticated
			
			String success_message = (String)session.getAttribute("success_message");
			if( success_message != ""){
				model.addAttribute("success_message",success_message);
				session.removeAttribute("success_message");
			}
			
			String error_message = (String)session.getAttribute("error_message");
			if( error_message != ""){
				model.addAttribute("error_message",error_message);
				session.removeAttribute("error_message");
			}
			
			//We'll get the logged in username
			String username = (String)session.getAttribute("account_username");
			
			
			//Then we'll check if the account already exists
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			account = accountJDBCTemplate.getAccountByUsername(username);
			
			if(account == null){
				
				success = false;
				message = "Account could not be found.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
				
			}
			
		}
		
		if(success == true){
			
			model.addAttribute("account_name", account.getName() );
			model.addAttribute("account_username", account.getUsername() );
			
			return "existing_account";
			
		}
		
		//If the script reaches here, it means something must have gone wrong
		session.setAttribute("error_message", message);
		model.addAttribute("redirect_url","/");
		return "redirect";
		
		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(HttpSession session, Model model){
		
		String error_message = (String)session.getAttribute("error_message");
		if( error_message != ""){
			model.addAttribute("error_message",error_message);
			session.removeAttribute("error_message");
		}
		return "register";
		
	}
	
}
