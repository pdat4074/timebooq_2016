package com.timebooq.app.web;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.timebooq.app.domain.Account.Account;
import com.timebooq.app.domain.Account.AccountJDBCTemplate;

@Controller
public class AccountController {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(
			HttpSession session
			, ModelMap model){
		
		Boolean success = true;
		String message = "Account logout was successful.";
		
		if(success == true){
			
			//Unset the session attributes
			
			session.removeAttribute("account_name");
			session.removeAttribute("account_username");
			session.removeAttribute("account_id");
			
			
			
			model.addAttribute("redirect_url","/");
			session.setAttribute("success_message", message);
			
		}else{
			
			model.addAttribute("redirect_url","/login");
			
		}
		
		return "redirect";
		
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String submitLogin(
			HttpSession session,
			@RequestParam("username") String username,
			@RequestParam("password") String password
			, ModelMap model){
		
		Boolean success = true;
		String message = "Account login was successful.";
		
		if(success == true){
			
			if(username.isEmpty() == true || password.isEmpty() == true){
				success = false;
				message = "Insufficient information provided.";
			}
			
		}
		
		if(success == true){
			
			//Then we'll check if the account already exists
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			Account account = accountJDBCTemplate.testCredentials(username, password);
			
			if(account == null){
				
				//This means that the credentials were incorrect
				success = false;
				message = "The username or password was incorrect";
				session.setAttribute("error_message", message);
				
			}
			
		}
		
		if(success == true){
			
			//This means that the account was successfully created
			
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			Account account = accountJDBCTemplate.getAccountByUsername(username);
			
			session.setAttribute("account_name", account.getName());
			session.setAttribute("account_username", account.getUsername());
			session.setAttribute("account_id", account.getId());
			
			model.addAttribute("redirect_url","/account/");
			session.setAttribute("success_message", message);
			
		}else{
			
			model.addAttribute("redirect_url","/login");
			
		}
		
		return "redirect";
		
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String submitRegister(
			HttpSession session,
			@RequestParam("name") String name,
			@RequestParam("username") String username,
			@RequestParam("password") String password
			, ModelMap model){
		
		Boolean success = true;
		String message = "The account was successfully created.";
		
		if(success == true){
			
			if(name.isEmpty() == true || username.isEmpty() == true || password.isEmpty() == true){
				success = false;
				message = "Insufficient information provided.";
			}
			
		}
		
		if(success == true){
			
			//Then we'll check if the account already exists
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			Account account = accountJDBCTemplate.getAccountByUsername(username);
			
			if(account != null){
				success = false;
				message = "An account with this username already exists";
				session.setAttribute("error_message", message);
			}
			
		}
		
		if(success == true){
			
			//This means we can proceed to create the account
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			Account account = accountJDBCTemplate.newAccount(name, username, password);
			
			if(account == null){
				
				//This means that the account could not be created
				success = false;
				message = "The account could not be created. Please try again.";
				session.setAttribute("error_message", message);
				
			}
			
		}
		
		if(success == true){
			
			//This means that the account was successfully created
			
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			Account account = accountJDBCTemplate.getAccountByUsername(username);
			
			session.setAttribute("account_name", account.getName());
			session.setAttribute("account_username", account.getUsername());
			session.setAttribute("account_id", account.getId());
			model.addAttribute("redirect_url","/account/");
			session.setAttribute("success_message", message);
			
		}else{
			
			model.addAttribute("redirect_url","/register");
			
		}
		
		return "redirect";
		
	}

}