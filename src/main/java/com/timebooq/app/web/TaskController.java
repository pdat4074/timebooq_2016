package com.timebooq.app.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.timebooq.app.domain.Account.Account;
import com.timebooq.app.domain.AccountTask.AccountTask;
import com.timebooq.app.domain.AccountTask.AccountTaskJDBCTemplate;
import com.timebooq.app.domain.Account.AccountJDBCTemplate;

@Controller
public class TaskController {
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	@RequestMapping(value = "/taskbooq", method = RequestMethod.GET)
	public String get_account_tasks(HttpSession session, Model model){
		
		//Set the default status
		Boolean success = true;
		String message = "Account tasks were displayed successfully";
		Account account = null;
		
		if(success == true){
			
			//We can only access this function if a user is logged in
			Long account_id = (Long)session.getAttribute("account_id");
			if( account_id == null){
				success = false;
				message = "User authentication required.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
			}
			
		}
		
		if(success == true){
			
			//This means that a user account is currently authenticated
			
			String success_message = (String)session.getAttribute("success_message");
			if( success_message != ""){
				model.addAttribute("success_message",success_message);
				session.removeAttribute("success_message");
			}
			
			String error_message = (String)session.getAttribute("error_message");
			if( error_message != ""){
				model.addAttribute("error_message",error_message);
				session.removeAttribute("error_message");
			}
			
			//We'll get the logged in username
			String username = (String)session.getAttribute("account_username");
			
			
			//Then we'll check if the account already exists
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			account = accountJDBCTemplate.getAccountByUsername(username);
			
			if(account == null){
				
				success = false;
				message = "Account could not be found.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
				
			}
			
		}
		
		if(success == true){
			
			model.addAttribute("account_name", account.getName() );
			model.addAttribute("account_username", account.getUsername() );
			
			//Then we'll get the tasks associated with this account
			ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

			AccountTaskJDBCTemplate accountTaskJDBCTemplate = (AccountTaskJDBCTemplate)context.getBean("accountTaskJDBCTemplate");
		  
			//Get the logged in account_id
			Long account_id = (Long)session.getAttribute("account_id");
			
			List<AccountTask> accountTasks = accountTaskJDBCTemplate.getAccountTasks(account_id);
			
			model.addAttribute("accountTasks", accountTasks);
			
			return "account_tasks";
			
		}
		
		//If the script reaches here, it means something must have gone wrong
		session.setAttribute("error_message", message);
		model.addAttribute("redirect_url","/");
		return "redirect";
		
		
	}
	
	@RequestMapping(value = "/taskbooq_add", method = RequestMethod.GET)
	public String addTask(HttpSession session, Model model){
		
		//Set the default status
		Boolean success = true;
		String message = "Account task add display was shown successfully";
		Account account = null;
		
		if(success == true){
			
			//We can only access this function if a user is logged in
			Long account_id = (Long)session.getAttribute("account_id");
			if( account_id == null){
				success = false;
				message = "User authentication required.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
			}
			
		}
		
		if(success == true){
			
			//This means that a user account is currently authenticated
			
			String success_message = (String)session.getAttribute("success_message");
			if( success_message != ""){
				model.addAttribute("success_message",success_message);
				session.removeAttribute("success_message");
			}
			
			String error_message = (String)session.getAttribute("error_message");
			if( error_message != ""){
				model.addAttribute("error_message",error_message);
				session.removeAttribute("error_message");
			}
			
			//We'll get the logged in username
			String username = (String)session.getAttribute("account_username");
			
			
			//Then we'll check if the account already exists
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			account = accountJDBCTemplate.getAccountByUsername(username);
			
			if(account == null){
				
				success = false;
				message = "Account could not be found.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
				
			}
			
		}
		
		if(success == true){
			
			model.addAttribute("account_name", account.getName() );
			model.addAttribute("account_username", account.getUsername() );
			
			return "taskbooq_add";
			
		}
		
		//If the script reaches here, it means something must have gone wrong
		session.setAttribute("error_message", message);
		model.addAttribute("redirect_url","/");
		return "redirect";
		
		
	}
	
	@RequestMapping(value = "/taskbooq_add", method = RequestMethod.POST)
	public String submitNewTask(
			HttpSession session,
			@RequestParam("name") String name,
			@RequestParam("due") String due,
			@RequestParam("description") String description,
			Model model){
		
		//Set the default status
		Boolean success = true;
		String message = "Account task was added successfully";
		Account account = null;
		
		if(success == true){
			
			//We can only access this function if a user is logged in
			Long account_id = (Long)session.getAttribute("account_id");
			if( account_id == null){
				success = false;
				message = "User authentication required.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
			}
			
		}
		
		if(success == true){
			
			//This means that a user account is currently authenticated
			
			String success_message = (String)session.getAttribute("success_message");
			if( success_message != ""){
				model.addAttribute("success_message",success_message);
				session.removeAttribute("success_message");
			}
			
			String error_message = (String)session.getAttribute("error_message");
			if( error_message != ""){
				model.addAttribute("error_message",error_message);
				session.removeAttribute("error_message");
			}
			
			//We'll get the logged in username
			String username = (String)session.getAttribute("account_username");
			
			
			//Then we'll check if the account already exists
			ApplicationContext context = 
		             new ClassPathXmlApplicationContext("beans.xml");

		      AccountJDBCTemplate accountJDBCTemplate = 
		      (AccountJDBCTemplate)context.getBean("accountJDBCTemplate");
			
			account = accountJDBCTemplate.getAccountByUsername(username);
			
			if(account == null){
				
				success = false;
				message = "Account could not be found.";
				session.setAttribute("error_message", message);
				model.addAttribute("redirect_url","/");
				return "redirect";
				
			}
			
		}
		
		if(success == true){
			
			//Then we'll add the new task to the database
			ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");

			AccountTaskJDBCTemplate accountTaskJDBCTemplate = (AccountTaskJDBCTemplate)context.getBean("accountTaskJDBCTemplate");
		  
			//Get the logged in account_id
			Long account_id = (Long)session.getAttribute("account_id");
			
			//Build the date
			String[] date_and_time = due.split(" ");
			String[] date_components = date_and_time[0].split("/");
			String[] time_components = date_and_time[1].split(":");
			String task_due = date_components[2]+'-'+date_components[1]+'-'+date_components[0]+' '+time_components[0]+':'+time_components[1]+":00"; 
			
			
			Boolean result = accountTaskJDBCTemplate.newAccountTask(account_id, name, task_due, description);
			if(result != true){
				success = false;
				message = "The new task could not be created";
			}
			
		}
		
		if(success == true){
			
			session.setAttribute("success_message", message);
			
		}else{
			session.setAttribute("error_message", message);
		}
		
		//If the script reaches here, it means something must have gone wrong
		
		model.addAttribute("redirect_url","/taskbooq");
		return "redirect";
		
		
	}
	
}