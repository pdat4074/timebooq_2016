<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ include file="/WEB-INF/views/account_header.jsp" %>
<div class="page-header">
    <h1>
    	My Facebook Data (${account_name })
    	<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			API connector
		</small>
    </h1>
</div>
<c:if test="${not empty error_message }">
	<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class='alert alert-danger'>${error_message}</div>
    </div>
</div>
</c:if>
<c:if test="${not empty success_message }">
	<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class='alert alert-success'>${success_message}</div>
    </div>
</div>
</c:if>
<%@ include file="/WEB-INF/views/account_footer.jsp" %>
