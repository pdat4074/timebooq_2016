<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ include file="/WEB-INF/views/login_header.jsp" %>
<h4 class="header blue lighter bigger center">
    <i class='ace-icon fa fa-shield'></i> New account created
</h4>
<div class='row'>
	<div class='col-xs-12'>
		<p>Account name: ${account_name}</p>
		<p>Account usrename: ${account_username}</p>
		<p>Account password: ${account_password}</p>
	</div>
</div>
<%@ include file="/WEB-INF/views/login_footer.jsp" %>
