<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ include file="/WEB-INF/views/account_header.jsp" %>
<div class='page-header'>
    <h1>Add Task to TaskBooq</h1>
</div>
<c:if test="${not empty error_message }">
	<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class='alert alert-danger'>${error_message}</div>
    </div>
</div>
</c:if>
<c:if test="${not empty success_message }">
	<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class='alert alert-success'>${success_message}</div>
    </div>
</div>
</c:if>
<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-6 col-lg-6'>
        <form id='add_api_connection' class='form-horizontal' method='post' action='taskbooq_add'>
            <p>Please fill out the following details to create a new task</p>
            <fieldset>
                <div class='form-group'>
                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                        <label class='control-label'>
                            Task name
                        </label>
                        <div class='clearfix'>
                            <input type='text' name='name' class='form-control' value=''/>
                        </div>
                    </div>
                </div>
                <div class='form-group'>
                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                        <label class='control-label'>
                            Due by
                        </label>
                        <div class='clearfix'>
                        	<div class="input-group">
								<input name='due' type="text" class="form-control date-timepicker" />
								<span class="input-group-addon">
									<i class="fa fa-clock-o bigger-110"></i>
								</span>
							</div>
                        </div>
                    </div>
                </div>
                <div class='form-group'>
                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                        <label class='control-label'>
                            Description
                        </label>
                        <div class='clearfix'>
                            <textarea name='description' class='form-control' value=''></textarea>
                        </div>
                    </div>
                </div>
                <div class="clearfix form-actions">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <button id='btn-add_api_connection' class="btn btn-info" type="submit">
                            <i class="ace-icon fa fa-check bigger-110"></i>
                            Submit
                        </button>
                        <button class="btn" type="reset">
                            <i class="ace-icon fa fa-undo bigger-110"></i>
                            Reset
                        </button>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class='row'>
	<div class='col-xs-12'>
		<hr/>
		<a href='/app/taskbooq' class='btn btn-inverse'><i class='ace-icon fa fa-arrow-left'></i> Back to task list</a>
	</div>
</div>
<%@ include file="/WEB-INF/views/account_footer.jsp" %>
