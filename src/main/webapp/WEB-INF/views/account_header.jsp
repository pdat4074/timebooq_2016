<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>TimeBooq</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <c:url value="/resources/css/bootstrap.min.css" var="bootstrap_min_css"/>
    <link rel="stylesheet" href="${bootstrap_min_css}" />
    <c:url value="/resources/css/font-awesome.min.css" var="fontawesome_min_css"/>
    <link rel="stylesheet" href="${fontawesome_min_css}" />

    <!-- text fonts -->
    <c:url value="/resources/css/ace-fonts.min.css" var="ace_fonts"/>
    <link rel="stylesheet" href="${ace_fonts}" />

    <!-- ace styles -->
    <c:url value="/resources/css/ace.min.css" var="ace_min"/>
    <link rel="stylesheet" href="${ace_min}" />

	<c:url value="/resources/favicon.png" var="favicon"/>
    <link rel="shortcut icon" href="${favicon}" />

    <c:url value="/resources/css/ace-part2.min.css" var="ace_part_2"/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ace_part_2}" />
    <![endif]-->

    <c:url value="/resources/css/ace-skins.min.css" var="ace_skins_min_css"/>
    <link rel="stylesheet" href="${ace_skins_min_css}" />
    
    <c:url value="/resources/css/ace-rtl.min.css" var="ace_rtl_min_css"/>
    <link rel="stylesheet" href="${ace_rtl_min_css}" />

    <c:url value="/resources/css/ace-ie.min.css" var="ace_ie_min_css"/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ace_ie_min_css}" />
    <![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <c:url value="/resources/js/html5shiv.min.js" var="html5shiv"/>
    <c:url value="/resources/js/respond.min.js" var="respond_min_js"/>
    <!--[if lt IE 8]>
    <script src="${html5shiv}"></script>
    <script src="${respond_min_js}"></script>
    <![endif]-->
    
    <!-- Page specific -->
    <c:url value="/resources/css/bootstrap-datetimepicker.min.css" var="bootstrap_datetimepicker"/>
    <link rel="stylesheet" href="${bootstrap_datetimepicker}" />
    
    <!-- Custom css -->
    <c:url value="/resources/custom/css/custom.css" var="custom_css"/>
    <link rel="stylesheet" href="${custom_css}" />
    
</head>
<body class='skin-1'>
    <div id="base_navbar" class="navbar navbar-default ace-save-state">
        <div class="navbar-container ace-save-state" id="base_navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#base_sidebar">
                <span class="sr-only">Toggle sidebar</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-header pull-left">
                <a href="#" class="navbar-brand">
                    <small>
                    	<c:url value="/resources/timebooq.png" var="timebooq_logo"/>
                        <span class="hidden-xs"><img height="20" src="${timebooq_logo}" style="position:relative; bottom:2px;"/> TimeBooq</span>
                    </small>
                </a>
            </div>
            <div class="navbar-buttons navbar-header pull-right" role='navigation'>
                <ul class="nav ace-nav">
                    <li class="dark-10 dropdown-modal">
                        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        	<c:url value="/resources/custom/img/app/default_user.jpg" var="default_user_image"/>
                            <img class="nav-user-photo" src="${default_user_image}" alt="Default user" />
                            <span class="user-info">
                                <small>Welcome,</small>
                                ${account_name}
                            </span>
                            <i class="ace-icon fa fa-caret-down"></i>
                        </a>

                        <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                            <li>
                                <a href="/app/logout">
                                    <i class="ace-icon fa fa-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="main-container" id="base_main_container">
        <div class='sidebar responsive' id='base_sidebar'>
            <ul class="nav nav-list">
            	<li class="">
                    <a href="/app/account">
                        <i class="menu-icon fa fa-tachometer home-icon"></i>
                        <span class="menu-text"> Dashbooq </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="#">
                        <i class="menu-icon fa fa-bed"></i>
                        <span class="menu-text"> SleepBooq </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/app/facebook/">
                        <i class="menu-icon fa fa-facebook"></i>
                        <span class="menu-text"> Facebook </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="#">
                        <i class="menu-icon fa fa-cogs"></i>
                        <span class="menu-text"> Activitybooq </span>
                    </a>

                    <b class="arrow"></b>
                </li>
                <li class="">
                    <a href="/app/taskbooq">
                        <i class="menu-icon fa fa-tasks"></i>
                        <span class="menu-text"> TaskBooq </span>
                    </a>

                    <b class="arrow"></b>
                </li>
            </ul><!-- /.nav-list -->
            <div data-target='#base_sidebar' class='sidebar-toggle sidebar-collapse'>
                <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
            </div>
        </div><!-- /.sidebar -->
        <div class="main-content">
            <div class="page-content">
                <div class='row'>
                    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>