<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>                        
                    </div>
                </div>
            </div>
        </div><!-- /.main-content -->
        <div class='footer'>
            <div class='footer-inner'>
                <div class='footer-content'>
                    � TimeBooq 2012 - 2016<br/>
                </div>
            </div>
        </div>
    </div>

    <!-- BASE JS -->

    <c:url value="/resources/js/jquery.min.js" var="jquery_min_js"/>
	<!--[if !IE]> -->
	<script src="${jquery_min_js}"></script>
	<!-- <![endif]-->
	<c:url value="/resources/js/jquery1x.min.js" var="jquery1x_min_js"/>
	<!--[if lte IE 9]>
	<script src="${jquery1x_min_js}"></script>
	<![endif]-->
	
	<c:url value="/resources/js/bootstrap.min.js" var="bootstrap_min_js"/>
	<script src="${bootstrap_min_js}"></script>
	
	<c:url value="/resources/js/excanvas.min.js" var="excanvas_min_js"/>
	<!-- ie8 canvas if required for plugins such as charts, etc -->
	<!--[if lte IE 8]>
	<script src="${excanvas_min_js}"></script>
	<![endif]-->
	
	<c:url value="/resources/js/ace-elements.min.js" var="ace_elements_min_js"/>
	<script src="${ace_elements_min_js}"></script>
	
	<c:url value="/resources/js/ace.min.js" var="ace_min_js"/>
	<script src="${ace_min_js}"></script>
	
	<c:url value="/resources/js/moment.min.js" var="moment_min_js"/>
	<script src="${moment_min_js}"></script>
	
	<c:url value="/resources/js/flot/jquery.flot.min.js" var="flot_min_js"/>
	<script src="${flot_min_js}"></script>
	
	<c:url value="/resources/js/flot/jquery.flot.pie.min.js" var="flot_pie_min_js"/>
	<script src="${flot_pie_min_js}"></script>
	
	<c:url value="/resources/js/flot/jquery.flot.resize.min.js" var="flot_resize_min_js"/>
	<script src="${flot_resize_min_js}"></script>
	
	<c:url value="/resources/js/bootstrap-datetimepicker.min.js" var="bootstrap_datetimepicker"/>
	<script src="${bootstrap_datetimepicker}"></script>
	<script type='text/javascript'>
		$(window).load(function(){
			$('.date-timepicker').datetimepicker({
				format: 'DD/MM/YYYY HH:mm',
		        pickSeconds: false,
		        pick12HourFormat: false            
		    })
		    
		    var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
			  var data = [
				{ label: "Social networking",  data: 38.7, color: "#68BC31"},
				{ label: "Work",  data: 24.5, color: "#2091CF"},
				{ label: "News",  data: 8.2, color: "#AF4E96"},
				{ label: "Entertainment",  data: 18.6, color: "#DA5430"},
				{ label: "Other",  data: 10, color: "#FEE074"}
			  ]
			  function drawPieChart(placeholder, data, position) {
			 	  $.plot(placeholder, data, {
					series: {
						pie: {
							show: true,
							tilt:0.8,
							highlight: {
								opacity: 0.25
							},
							stroke: {
								color: '#fff',
								width: 2
							},
							startAngle: 2
						}
					},
					legend: {
						show: true,
						position: position || "ne", 
						labelBoxBorderColor: null,
						margin:[-30,15]
					}
					,
					grid: {
						hoverable: true,
						clickable: true
					}
				 })
			 }
			 drawPieChart(placeholder, data);
			
			 /**
			 we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
			 so that's not needed actually.
			 */
			 placeholder.data('chart', data);
			 placeholder.data('draw', drawPieChart);
			
			
			  //pie chart tooltip example
			  var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
			  var previousPoint = null;
			
			  placeholder.on('plothover', function (event, pos, item) {
				if(item) {
					if (previousPoint != item.seriesIndex) {
						previousPoint = item.seriesIndex;
						var tip = item.series['label'] + " : " + item.series['percent']+'%';
						$tooltip.show().children(0).text(tip);
					}
					$tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
				} else {
					$tooltip.hide();
					previousPoint = null;
				}
				
			 });
			
				/////////////////////////////////////
				$(document).one('ajaxloadstart.page', function(e) {
					$tooltip.remove();
				});
			
				var d1 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d1.push([i, Math.sin(i)]);
				}
			
				var d2 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.5) {
					d2.push([i, Math.cos(i)]);
				}
			
				var d3 = [];
				for (var i = 0; i < Math.PI * 2; i += 0.2) {
					d3.push([i, Math.tan(i)]);
				}
				
			
				var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
				$.plot("#sales-charts", [
					{ label: "Domains", data: d1 },
					{ label: "Hosting", data: d2 },
					{ label: "Services", data: d3 }
				], {
					hoverable: true,
					shadowSize: 0,
					series: {
						lines: { show: true },
						points: { show: true }
					},
					xaxis: {
						tickLength: 0
					},
					yaxis: {
						ticks: 10,
						min: -2,
						max: 2,
						tickDecimals: 3
					},
					grid: {
						backgroundColor: { colors: [ "#fff", "#fff" ] },
						borderWidth: 1,
						borderColor:'#555'
					}
				});
		})
	</script>

</body>
</html>
