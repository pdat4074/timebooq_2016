<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>TimeBooq</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- bootstrap & fontawesome -->
    <c:url value="/resources/css/bootstrap.min.css" var="bootstrap_min_css"/>
    <link rel="stylesheet" href="${bootstrap_min_css}" />
    <c:url value="/resources/css/font-awesome.min.css" var="fontawesome_min_css"/>
    <link rel="stylesheet" href="${fontawesome_min_css}" />

    <!-- text fonts -->
    <c:url value="/resources/css/ace-fonts.min.css" var="ace_fonts"/>
    <link rel="stylesheet" href="${ace_fonts}" />

    <!-- ace styles -->
    <c:url value="/resources/css/ace.min.css" var="ace_min"/>
    <link rel="stylesheet" href="${ace_min}" />

	<c:url value="/resources/favicon.png" var="favicon"/>
    <link rel="shortcut icon" href="${favicon}" />

    <c:url value="/resources/css/ace-part2.min.css" var="ace_part_2"/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ace_part_2}" />
    <![endif]-->

    <c:url value="/resources/css/ace-skins.min.css" var="ace_skins_min_css"/>
    <link rel="stylesheet" href="${ace_skins_min_css}" />
    
    <c:url value="/resources/css/ace-rtl.min.css" var="ace_rtl_min_css"/>
    <link rel="stylesheet" href="${ace_rtl_min_css}" />

    <c:url value="/resources/css/ace-ie.min.css" var="ace_ie_min_css"/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="${ace_ie_min_css}" />
    <![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <c:url value="/resources/js/html5shiv.min.js" var="html5shiv"/>
    <c:url value="/resources/js/respond.min.js" var="respond_min_js"/>
    <!--[if lt IE 8]>
    <script src="${html5shiv}"></script>
    <script src="${respond_min_js}"></script>
    <![endif]-->
</head>

<body class="login-layout">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                    	<c:url value="/resources/timebooq.png" var="timebooq_logo"/>
                    	<img style='width:100%' src='${timebooq_logo}'/>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <div class='container-fluid'>