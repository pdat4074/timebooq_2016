<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ include file="/WEB-INF/views/account_header.jsp" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<div class="page-header">
    <h1>
    	Account tasks
    	<small>
			<i class="ace-icon fa fa-angle-double-right"></i>
			Overview &amp; stats
		</small>
    </h1>
</div>
<c:if test="${not empty error_message }">
	<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class='alert alert-danger'>${error_message}</div>
    </div>
</div>
</c:if>
<c:if test="${not empty success_message }">
	<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class='alert alert-success'>${success_message}</div>
    </div>
</div>
</c:if>
<div class='row'>
	<div class='col-xs-12'>
		<table class='table table-bordered table-striped'>
			<thead>
				<th>Name</th>
				<th>Due</th>
				<th>Description</th>
				<th>Status</th>
			</thead>
			<tbody>
				<c:forEach items="${accountTasks}" var="accountTask">
				    <tr>
				        <td><c:out value="${accountTask.name}"/></td>
				        <td><fmt:formatDate pattern="dd/MM/yyyy" value="${accountTask.due}"/></td>  
				        <td><c:out value="${accountTask.description}"/></td>
				        <td><c:out value="${accountTask.status}"/></td>
				    </tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>
<div class='row'>
	<div class='col-xs-12'>
		<a href='/app/taskbooq_add' class='btn btn-success'>Add new</a>
	</div>
</div>
<%@ include file="/WEB-INF/views/account_footer.jsp" %>
