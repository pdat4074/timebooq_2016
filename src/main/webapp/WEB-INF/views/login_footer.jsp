									<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
									</div>
                                </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->
                    </div><!-- /.position-relative -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.main-content -->
</div><!-- /.main-container -->

<!-- BASE JS -->

<c:url value="/resources/js/jquery.min.js" var="jquery_min_js"/>
<!--[if !IE]> -->
<script src="${jquery_min_js}"></script>
<!-- <![endif]-->
<c:url value="/resources/js/jquery1x.min.js" var="jquery1x_min_js"/>
<!--[if lte IE 9]>
<script src="${jquery1x_min_js}"></script>
<![endif]-->

<c:url value="/resources/js/bootstrap.min.js" var="bootstrap_min_js"/>
<script src="${bootstrap_min_js}"></script>

<c:url value="/resources/js/excanvas.min.js" var="excanvas_min_js"/>
<!-- ie8 canvas if required for plugins such as charts, etc -->
<!--[if lte IE 8]>
<script src="${excanvas_min_js}"></script>
<![endif]-->

<c:url value="/resources/js/ace-elements.min.js" var="ace_elements_min_js"/>
<script src="${ace_elements_min_js}"></script>

<c:url value="/resources/js/ace.min.js" var="ace_min_js"/>
<script src="${ace_min_js}"></script>

</body>
</html>