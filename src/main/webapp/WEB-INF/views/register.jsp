<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ include file="/WEB-INF/views/login_header.jsp" %>
<h4 class="header green lighter bigger center">
    <i class='ace-icon fa fa-user-plus'></i> Create your account today
</h4>
<c:if test="${not empty error_message }">
	<div class='row'>
    <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
        <div class='alert alert-danger'>${error_message}</div>
    </div>
</div>
</c:if>
<form id='registration_form' role='form' action="register" method="post">
    <fieldset>
    	<div class='form-group'>
            <label class="block clearfix">
            <span class="block input-icon input-icon-right">
                <input type="text" class="form-control" placeholder="Name" name="name" value=""/>
                <i class="ace-icon fa fa-user"></i>
            </span>
            </label>
        </div>
        <div class='form-group'>
            <label class="block clearfix">
            <span class="block input-icon input-icon-right">
                <input type="text" class="form-control" placeholder="ID" name="username" value=""/>
                <i class="ace-icon fa fa-user"></i>
            </span>
            </label>
        </div>
        <div class='form-group'>
            <label class="block clearfix">
            <span class="block input-icon input-icon-right">
                <input type="password" class="form-control" placeholder="Password" name="password" value=""/>
                <i class="ace-icon fa fa-lock"></i>
            </span>
            </label>
        </div>
        <div class="space"></div>
        <div class="clearfix">
            <button type="submit" class="btn btn-block btn-sm btn-success">
                Submit
                <i class="ace-icon fa fa-arrow-right"></i>
            </button>
            <a href="login" class="btn btn-block btn-sm btn-inverse">
                <i class="ace-icon fa fa-arrow-left"></i>
                Back to login
            </a>
        </div>
    </fieldset>
</form>
<%@ include file="/WEB-INF/views/login_footer.jsp" %>
